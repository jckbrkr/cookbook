# Cookbook

## Compte de test

Nom d'utilisateur : admin

Mot de passe : admin1234

## Fonctionnalités

Utilisateurs standard:

* enregistrement

* connexion

* afficher les recettes

Utilisateurs administrateurs:

* enregistrement

* connexion

* afficher les recettes

* ajouter des recettes

* modifier / supprimer les recettes
