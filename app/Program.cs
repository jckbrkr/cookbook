﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using app.Views;
using app.DataSource;
using app.Models;

namespace app
{
    static class Program
    {

        private static DataProvider dataProvider = new DataProvider();

        public static DataProvider GetDataProvider()
        {
            return dataProvider;
        }

        /// <summary>
        /// Application entry point.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new LoginForm());
        }
        
    }
}
