﻿using app.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace app.Views
{
    public partial class ProfileForm : Form
    {
        UserController userController = new UserController();

        public ProfileForm()
        {
            InitializeComponent();
            InitializeForm();
        }

        public void InitializeForm()
        {
            this.lblUsernameData.Text = Globals.GetSignedInUser().GetUsername();
        }

        private void ShowErrorMessageAndClearTextBoxes(string message)
        {
            MessageBox.Show(
               message + " Veuillez réessayer.",
               "Echec de l'enregistrement",
               MessageBoxButtons.OK,
               MessageBoxIcon.Warning
           );
            this.tbPassword.Text = "";
            this.tbPasswordConfirm.Text = "";
        }

        private void btnConfirmPasswordChange_Click(object sender, EventArgs e)
        {
            string password = this.tbPassword.Text;
            string passwordConfirmation = this.tbPasswordConfirm.Text;

            // check password confirmation (already checked in the form, this is for extra safety)
            if (password != passwordConfirmation)
                ShowErrorMessageAndClearTextBoxes("Confirmation du mot de passe incorrecte.");

            // check password length (already checked in the form, this is for extra safety)
            if (password.Length < 8)
                ShowErrorMessageAndClearTextBoxes("Mot de passe trop court.");

            // pass data to the UserController for SignUp()
            string username = Globals.GetSignedInUser().GetUsername();
            if (this.userController.SetUserPassword(username, password))
            {
                new HomeForm().Show();
                this.Hide();
            }
            else
            {
                this.ShowErrorMessageAndClearTextBoxes("Echec de l'enregistrement.");
            }
        }
    }
}
