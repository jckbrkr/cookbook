﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using app.Models;

namespace app.Views
{
    public partial class HomeForm : Form
    {
        public HomeForm()
        {
            InitializeComponent();
        }

        private void btnRecipes_Click(object sender, EventArgs e)
        {
            new RecipesForm().Show();
            this.Hide();
        }

        private void btnProfile_Click(object sender, EventArgs e)
        {
            new ProfileForm().Show();
            this.Hide();
        }
    }
}
