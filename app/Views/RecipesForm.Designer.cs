﻿using app.Views.Recipes;

namespace app.Views
{
    partial class RecipesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.scRecipes = new System.Windows.Forms.SplitContainer();
            this.lblAllRecipes = new System.Windows.Forms.Label();
            this.lblFavoriteRecipes = new System.Windows.Forms.Label();
            this.lbFavoriteRecipes = new System.Windows.Forms.ListBox();
            this.btnRecipesClose = new System.Windows.Forms.Button();
            this.btnEditRecipe = new System.Windows.Forms.Button();
            this.btnAddRecipe = new System.Windows.Forms.Button();
            this.lbRecipes = new System.Windows.Forms.ListBox();
            this.chbFavorite = new System.Windows.Forms.CheckBox();
            this.lbRecipeIngredients = new System.Windows.Forms.ListBox();
            this.lblRecipeDescription = new System.Windows.Forms.Label();
            this.lblRecipeName = new System.Windows.Forms.Label();
            this.lblRecipeIngredients = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.scRecipes)).BeginInit();
            this.scRecipes.Panel1.SuspendLayout();
            this.scRecipes.Panel2.SuspendLayout();
            this.scRecipes.SuspendLayout();
            this.SuspendLayout();
            // 
            // scRecipes
            // 
            this.scRecipes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scRecipes.Location = new System.Drawing.Point(0, 0);
            this.scRecipes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.scRecipes.Name = "scRecipes";
            // 
            // scRecipes.Panel1
            // 
            this.scRecipes.Panel1.Controls.Add(this.lblAllRecipes);
            this.scRecipes.Panel1.Controls.Add(this.lblFavoriteRecipes);
            this.scRecipes.Panel1.Controls.Add(this.lbFavoriteRecipes);
            this.scRecipes.Panel1.Controls.Add(this.btnRecipesClose);
            this.scRecipes.Panel1.Controls.Add(this.btnEditRecipe);
            this.scRecipes.Panel1.Controls.Add(this.btnAddRecipe);
            this.scRecipes.Panel1.Controls.Add(this.lbRecipes);
            // 
            // scRecipes.Panel2
            // 
            this.scRecipes.Panel2.Controls.Add(this.lblDescription);
            this.scRecipes.Panel2.Controls.Add(this.lblRecipeIngredients);
            this.scRecipes.Panel2.Controls.Add(this.chbFavorite);
            this.scRecipes.Panel2.Controls.Add(this.lbRecipeIngredients);
            this.scRecipes.Panel2.Controls.Add(this.lblRecipeDescription);
            this.scRecipes.Panel2.Controls.Add(this.lblRecipeName);
            this.scRecipes.Size = new System.Drawing.Size(1200, 623);
            this.scRecipes.SplitterDistance = 393;
            this.scRecipes.SplitterWidth = 6;
            this.scRecipes.TabIndex = 0;
            this.scRecipes.TabStop = false;
            // 
            // lblAllRecipes
            // 
            this.lblAllRecipes.AutoSize = true;
            this.lblAllRecipes.Location = new System.Drawing.Point(6, 253);
            this.lblAllRecipes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAllRecipes.Name = "lblAllRecipes";
            this.lblAllRecipes.Size = new System.Drawing.Size(148, 18);
            this.lblAllRecipes.TabIndex = 6;
            this.lblAllRecipes.Text = "Toutes les recettes";
            // 
            // lblFavoriteRecipes
            // 
            this.lblFavoriteRecipes.AutoSize = true;
            this.lblFavoriteRecipes.Location = new System.Drawing.Point(6, 6);
            this.lblFavoriteRecipes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFavoriteRecipes.Name = "lblFavoriteRecipes";
            this.lblFavoriteRecipes.Size = new System.Drawing.Size(90, 18);
            this.lblFavoriteRecipes.TabIndex = 5;
            this.lblFavoriteRecipes.Text = "Vos favoris";
            // 
            // lbFavoriteRecipes
            // 
            this.lbFavoriteRecipes.FormattingEnabled = true;
            this.lbFavoriteRecipes.ItemHeight = 18;
            this.lbFavoriteRecipes.Location = new System.Drawing.Point(6, 28);
            this.lbFavoriteRecipes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lbFavoriteRecipes.Name = "lbFavoriteRecipes";
            this.lbFavoriteRecipes.Size = new System.Drawing.Size(380, 220);
            this.lbFavoriteRecipes.TabIndex = 6;
            this.lbFavoriteRecipes.TabStop = false;
            this.lbFavoriteRecipes.SelectedIndexChanged += new System.EventHandler(this.lbFavoriteRecipes_SelectedIndexChanged);
            // 
            // btnRecipesClose
            // 
            this.btnRecipesClose.Location = new System.Drawing.Point(6, 586);
            this.btnRecipesClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRecipesClose.Name = "btnRecipesClose";
            this.btnRecipesClose.Size = new System.Drawing.Size(382, 32);
            this.btnRecipesClose.TabIndex = 3;
            this.btnRecipesClose.Text = "&Fermer";
            this.btnRecipesClose.UseVisualStyleBackColor = true;
            this.btnRecipesClose.Click += new System.EventHandler(this.btnRecipesReturn_Click);
            // 
            // btnEditRecipe
            // 
            this.btnEditRecipe.Location = new System.Drawing.Point(6, 546);
            this.btnEditRecipe.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnEditRecipe.Name = "btnEditRecipe";
            this.btnEditRecipe.Size = new System.Drawing.Size(382, 32);
            this.btnEditRecipe.TabIndex = 2;
            this.btnEditRecipe.Text = "&Modifier";
            this.btnEditRecipe.UseVisualStyleBackColor = true;
            this.btnEditRecipe.Click += new System.EventHandler(this.btnEditRecipe_Click);
            // 
            // btnAddRecipe
            // 
            this.btnAddRecipe.Location = new System.Drawing.Point(6, 505);
            this.btnAddRecipe.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddRecipe.Name = "btnAddRecipe";
            this.btnAddRecipe.Size = new System.Drawing.Size(382, 32);
            this.btnAddRecipe.TabIndex = 1;
            this.btnAddRecipe.Text = "&Ajouter";
            this.btnAddRecipe.UseVisualStyleBackColor = true;
            this.btnAddRecipe.Click += new System.EventHandler(this.btnAddRecipe_Click);
            // 
            // lbRecipes
            // 
            this.lbRecipes.FormattingEnabled = true;
            this.lbRecipes.ItemHeight = 18;
            this.lbRecipes.Location = new System.Drawing.Point(6, 276);
            this.lbRecipes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lbRecipes.Name = "lbRecipes";
            this.lbRecipes.Size = new System.Drawing.Size(380, 220);
            this.lbRecipes.TabIndex = 7;
            this.lbRecipes.TabStop = false;
            this.lbRecipes.SelectedIndexChanged += new System.EventHandler(this.lbRecipes_SelectedIndexChanged);
            // 
            // chbFavorite
            // 
            this.chbFavorite.AutoSize = true;
            this.chbFavorite.Location = new System.Drawing.Point(9, 35);
            this.chbFavorite.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chbFavorite.Name = "chbFavorite";
            this.chbFavorite.Size = new System.Drawing.Size(72, 22);
            this.chbFavorite.TabIndex = 4;
            this.chbFavorite.Text = "&Favori";
            this.chbFavorite.UseVisualStyleBackColor = true;
            this.chbFavorite.Click += new System.EventHandler(this.chbFavorite_Click);
            // 
            // lbRecipeIngredients
            // 
            this.lbRecipeIngredients.BackColor = System.Drawing.SystemColors.Control;
            this.lbRecipeIngredients.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbRecipeIngredients.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRecipeIngredients.FormattingEnabled = true;
            this.lbRecipeIngredients.ItemHeight = 18;
            this.lbRecipeIngredients.Location = new System.Drawing.Point(8, 278);
            this.lbRecipeIngredients.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lbRecipeIngredients.Name = "lbRecipeIngredients";
            this.lbRecipeIngredients.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbRecipeIngredients.Size = new System.Drawing.Size(779, 126);
            this.lbRecipeIngredients.TabIndex = 5;
            this.lbRecipeIngredients.TabStop = false;
            // 
            // lblRecipeDescription
            // 
            this.lblRecipeDescription.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecipeDescription.Location = new System.Drawing.Point(6, 118);
            this.lblRecipeDescription.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRecipeDescription.Name = "lblRecipeDescription";
            this.lblRecipeDescription.Size = new System.Drawing.Size(781, 110);
            this.lblRecipeDescription.TabIndex = 1;
            this.lblRecipeDescription.Text = "lblRecipeDescription";
            // 
            // lblRecipeName
            // 
            this.lblRecipeName.AutoSize = true;
            this.lblRecipeName.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecipeName.Location = new System.Drawing.Point(4, 12);
            this.lblRecipeName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRecipeName.Name = "lblRecipeName";
            this.lblRecipeName.Size = new System.Drawing.Size(150, 20);
            this.lblRecipeName.TabIndex = 0;
            this.lblRecipeName.Text = "lblRecipeName";
            // 
            // lblRecipeIngredients
            // 
            this.lblRecipeIngredients.AutoSize = true;
            this.lblRecipeIngredients.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecipeIngredients.Location = new System.Drawing.Point(5, 253);
            this.lblRecipeIngredients.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRecipeIngredients.Name = "lblRecipeIngredients";
            this.lblRecipeIngredients.Size = new System.Drawing.Size(120, 20);
            this.lblRecipeIngredients.TabIndex = 6;
            this.lblRecipeIngredients.Text = "Ingredients";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Location = new System.Drawing.Point(5, 98);
            this.lblDescription.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(118, 20);
            this.lblDescription.TabIndex = 7;
            this.lblDescription.Text = "Description";
            // 
            // RecipesForm
            // 
            this.AcceptButton = this.btnAddRecipe;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 623);
            this.Controls.Add(this.scRecipes);
            this.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "RecipesForm";
            this.Text = "Mes recettes";
            this.scRecipes.Panel1.ResumeLayout(false);
            this.scRecipes.Panel1.PerformLayout();
            this.scRecipes.Panel2.ResumeLayout(false);
            this.scRecipes.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scRecipes)).EndInit();
            this.scRecipes.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer scRecipes;
        private System.Windows.Forms.ListBox lbRecipes;
        private System.Windows.Forms.Label lblRecipeName;
        private System.Windows.Forms.Label lblRecipeDescription;
        private System.Windows.Forms.Button btnAddRecipe;
        private System.Windows.Forms.Button btnEditRecipe;
        private System.Windows.Forms.Button btnRecipesClose;
        private System.Windows.Forms.ListBox lbRecipeIngredients;
        private System.Windows.Forms.ListBox lbFavoriteRecipes;
        private System.Windows.Forms.Label lblFavoriteRecipes;
        private System.Windows.Forms.Label lblAllRecipes;
        private System.Windows.Forms.CheckBox chbFavorite;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblRecipeIngredients;
    }
}