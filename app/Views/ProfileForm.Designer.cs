﻿namespace app.Views
{
    partial class ProfileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbPasswordConfirm = new System.Windows.Forms.TextBox();
            this.lblPasswordConfirm = new System.Windows.Forms.Label();
            this.btnConfirmPasswordChange = new System.Windows.Forms.Button();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblUsernameData = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbPasswordConfirm
            // 
            this.tbPasswordConfirm.Location = new System.Drawing.Point(130, 62);
            this.tbPasswordConfirm.Name = "tbPasswordConfirm";
            this.tbPasswordConfirm.PasswordChar = '*';
            this.tbPasswordConfirm.Size = new System.Drawing.Size(238, 20);
            this.tbPasswordConfirm.TabIndex = 2;
            // 
            // lblPasswordConfirm
            // 
            this.lblPasswordConfirm.AutoSize = true;
            this.lblPasswordConfirm.Location = new System.Drawing.Point(12, 65);
            this.lblPasswordConfirm.Name = "lblPasswordConfirm";
            this.lblPasswordConfirm.Size = new System.Drawing.Size(71, 13);
            this.lblPasswordConfirm.TabIndex = 19;
            this.lblPasswordConfirm.Text = "Mot de passe";
            // 
            // btnConfirmPasswordChange
            // 
            this.btnConfirmPasswordChange.Location = new System.Drawing.Point(12, 98);
            this.btnConfirmPasswordChange.Name = "btnConfirmPasswordChange";
            this.btnConfirmPasswordChange.Size = new System.Drawing.Size(356, 23);
            this.btnConfirmPasswordChange.TabIndex = 3;
            this.btnConfirmPasswordChange.Text = "Confirmer";
            this.btnConfirmPasswordChange.UseVisualStyleBackColor = true;
            this.btnConfirmPasswordChange.Click += new System.EventHandler(this.btnConfirmPasswordChange_Click);
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(130, 35);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(238, 20);
            this.tbPassword.TabIndex = 1;
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(12, 38);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(71, 13);
            this.lblPassword.TabIndex = 15;
            this.lblPassword.Text = "Mot de passe";
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Location = new System.Drawing.Point(12, 10);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(84, 13);
            this.lblUsername.TabIndex = 14;
            this.lblUsername.Text = "Nom d\'utilisateur";
            // 
            // lblUsernameData
            // 
            this.lblUsernameData.AutoSize = true;
            this.lblUsernameData.Location = new System.Drawing.Point(127, 9);
            this.lblUsernameData.Name = "lblUsernameData";
            this.lblUsernameData.Size = new System.Drawing.Size(59, 13);
            this.lblUsernameData.TabIndex = 21;
            this.lblUsernameData.Text = "[username]";
            // 
            // ProfileForm
            // 
            this.AcceptButton = this.btnConfirmPasswordChange;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 129);
            this.Controls.Add(this.lblUsernameData);
            this.Controls.Add(this.tbPasswordConfirm);
            this.Controls.Add(this.lblPasswordConfirm);
            this.Controls.Add(this.btnConfirmPasswordChange);
            this.Controls.Add(this.tbPassword);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblUsername);
            this.Name = "ProfileForm";
            this.Text = "Changement de mot de passe";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbPasswordConfirm;
        private System.Windows.Forms.Label lblPasswordConfirm;
        private System.Windows.Forms.Button btnConfirmPasswordChange;
        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblUsernameData;
    }
}