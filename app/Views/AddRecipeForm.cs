﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using app.Models;
using app.DataSource;

namespace app.Views.Recipes
{
    public partial class AddRecipeForm : Form
    {
        private DataProvider dataProvider;
        private RecipesForm recipesForm;

        BindingList<Ingredient> availableIngredients;
        BindingList<Ingredient> selectedIngredients;

        public AddRecipeForm(RecipesForm recipesForm)
        {
            InitializeComponent();
            this.dataProvider = Program.GetDataProvider();
            this.recipesForm = recipesForm;
            this.availableIngredients = new BindingList<Ingredient>(this.dataProvider.GetIngredients().ToList());
            this.selectedIngredients = new BindingList<Ingredient>();
            InitializeForm();
        }

        public void InitializeForm()
        {
            this.lbAvailableIngredients.DataSource = this.availableIngredients;
            this.lbSelectedIngredients.DataSource = this.selectedIngredients;
        }

        // on cancel, do nothing and show recipe form
        private void btnAddRecipeCancel_Click(object sender, EventArgs e)
        {
            new RecipesForm().Show();
            this.Close();
        }

        // handle add recipe
        private void btnAddRecipeSave_Click(object sender, EventArgs e)
        {
            string newRecipeName = tbAddRecipeName.Text;
            string newRecipeDescription = tbAddRecipeDescription.Text;
            // get next id for dummy datastore
            int nextId = this.dataProvider.recipes.Count();

            // instanciate new recipe
            Recipe newRecipe = new Recipe(nextId, newRecipeName, newRecipeDescription, this.selectedIngredients);

            // persist
            this.dataProvider.AddRecipe(newRecipe);

            // re init recipes form
            this.recipesForm.InitializeForm();

            new RecipesForm().Show();
            this.Close();
        }

        private void btnAddIngredient_Click(object sender, EventArgs e)
        {
            selectedIngredients.Add((Ingredient)lbAvailableIngredients.SelectedItem);
            availableIngredients.Remove((Ingredient)lbAvailableIngredients.SelectedItem);
        }

        private void btnRemoveIngredient_Click(object sender, EventArgs e)
        {
            availableIngredients.Add((Ingredient)lbSelectedIngredients.SelectedItem);
            selectedIngredients.Remove((Ingredient)lbSelectedIngredients.SelectedItem);
        }

        private void btnNewIngredient_Click(object sender, EventArgs e)
        {
            new AddIngredientForm().Show();
        }
    }
}
