﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using app.DataSource;
using app.Models;

namespace app.Views.Recipes
{
    public partial class EditRecipeForm : Form
    {
        Recipe recipe;
        DataProvider dataProvider = Program.GetDataProvider();
        BindingList<Ingredient> selectedIngredients;
        BindingList<Ingredient> availableIngredients;

        public EditRecipeForm(Recipe recipe)
        {
            InitializeComponent();
            this.recipe = recipe;
            this.InitializeForm();
            this.availableIngredients = new BindingList<Ingredient>(this.dataProvider.GetIngredients().ToList());
        }

        private void InitializeForm()
        {
            tbEditRecipeName.Text = this.recipe.name;
            tbEditRecipeDescription.Text = this.recipe.description;
            lbAvailableIngredients.DataSource = this.dataProvider.GetIngredients();
            this.selectedIngredients = this.recipe.ingredients;
            lbSelectedIngredients.DataSource = this.selectedIngredients;
        }

        // edit recipe on save, close form and open recipe form
        private void btnEditRecipeSave_Click(object sender, EventArgs e)
        {
            this.recipe.name = tbEditRecipeName.Text;
            this.recipe.description = tbEditRecipeDescription.Text;
            this.recipe.ingredients = this.selectedIngredients;
            
            new RecipesForm().Show();
            this.Close();
        }

        // persist nothing on form cancel, close form and open recipe form
        private void btnEditRecipeCancel_Click(object sender, EventArgs e)
        {
            new RecipesForm().Show();
            this.Close();
        }

        // remove recipe from datastore, close form and open recipe form
        private void btnEditRecipeDelete_Click(object sender, EventArgs e)
        {
            Globals.GetSignedInUser().RemoveFavoriteRecipe(this.recipe);
            this.dataProvider.recipes.Remove(this.recipe);
            new RecipesForm().Show();
            this.Close();
        }

        // handle ingredient adding
        private void btnAddIngredient_Click(object sender, EventArgs e)
        {
            selectedIngredients.Add((Ingredient)lbAvailableIngredients.SelectedItem);
            availableIngredients.Remove((Ingredient)lbAvailableIngredients.SelectedItem);
        }

        // handle ingredient removal
        private void btnRemoveIngredient_Click(object sender, EventArgs e)
        {
            availableIngredients.Add((Ingredient)lbSelectedIngredients.SelectedItem);
            selectedIngredients.Remove((Ingredient)lbSelectedIngredients.SelectedItem);
        }

        // show new ingredient form
        private void btnNewIngredient_Click(object sender, EventArgs e)
        {
            new AddIngredientForm().Show();
        }
    }
}
