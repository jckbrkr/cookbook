﻿namespace app.Views.Recipes
{
    partial class AddRecipeForm
    {
        
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAddRecipeName = new System.Windows.Forms.Label();
            this.lblAddRecipeDescription = new System.Windows.Forms.Label();
            this.tbAddRecipeName = new System.Windows.Forms.TextBox();
            this.tbAddRecipeDescription = new System.Windows.Forms.TextBox();
            this.btnAddRecipeSave = new System.Windows.Forms.Button();
            this.btnAddRecipeCancel = new System.Windows.Forms.Button();
            this.lbAvailableIngredients = new System.Windows.Forms.ListBox();
            this.lblAddRecipeIngredientsCatalog = new System.Windows.Forms.Label();
            this.btnRemoveIngredient = new System.Windows.Forms.Button();
            this.lbSelectedIngredients = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddIngredient = new System.Windows.Forms.Button();
            this.btnNewIngredient = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblAddRecipeName
            // 
            this.lblAddRecipeName.AutoSize = true;
            this.lblAddRecipeName.Location = new System.Drawing.Point(18, 12);
            this.lblAddRecipeName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddRecipeName.Name = "lblAddRecipeName";
            this.lblAddRecipeName.Size = new System.Drawing.Size(44, 18);
            this.lblAddRecipeName.TabIndex = 0;
            this.lblAddRecipeName.Text = "Nom";
            // 
            // lblAddRecipeDescription
            // 
            this.lblAddRecipeDescription.AutoSize = true;
            this.lblAddRecipeDescription.Location = new System.Drawing.Point(18, 58);
            this.lblAddRecipeDescription.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddRecipeDescription.Name = "lblAddRecipeDescription";
            this.lblAddRecipeDescription.Size = new System.Drawing.Size(90, 18);
            this.lblAddRecipeDescription.TabIndex = 1;
            this.lblAddRecipeDescription.Text = "Description";
            // 
            // tbAddRecipeName
            // 
            this.tbAddRecipeName.Location = new System.Drawing.Point(196, 8);
            this.tbAddRecipeName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbAddRecipeName.Name = "tbAddRecipeName";
            this.tbAddRecipeName.Size = new System.Drawing.Size(404, 26);
            this.tbAddRecipeName.TabIndex = 1;
            // 
            // tbAddRecipeDescription
            // 
            this.tbAddRecipeDescription.Location = new System.Drawing.Point(196, 54);
            this.tbAddRecipeDescription.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbAddRecipeDescription.Name = "tbAddRecipeDescription";
            this.tbAddRecipeDescription.Size = new System.Drawing.Size(404, 26);
            this.tbAddRecipeDescription.TabIndex = 2;
            // 
            // btnAddRecipeSave
            // 
            this.btnAddRecipeSave.Location = new System.Drawing.Point(18, 476);
            this.btnAddRecipeSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddRecipeSave.Name = "btnAddRecipeSave";
            this.btnAddRecipeSave.Size = new System.Drawing.Size(296, 32);
            this.btnAddRecipeSave.TabIndex = 6;
            this.btnAddRecipeSave.Text = "&Enregistrer";
            this.btnAddRecipeSave.UseVisualStyleBackColor = true;
            this.btnAddRecipeSave.Click += new System.EventHandler(this.btnAddRecipeSave_Click);
            // 
            // btnAddRecipeCancel
            // 
            this.btnAddRecipeCancel.Location = new System.Drawing.Point(322, 476);
            this.btnAddRecipeCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddRecipeCancel.Name = "btnAddRecipeCancel";
            this.btnAddRecipeCancel.Size = new System.Drawing.Size(280, 32);
            this.btnAddRecipeCancel.TabIndex = 7;
            this.btnAddRecipeCancel.Text = "Ann&uler";
            this.btnAddRecipeCancel.UseVisualStyleBackColor = true;
            this.btnAddRecipeCancel.Click += new System.EventHandler(this.btnAddRecipeCancel_Click);
            // 
            // lbAvailableIngredients
            // 
            this.lbAvailableIngredients.FormattingEnabled = true;
            this.lbAvailableIngredients.ItemHeight = 18;
            this.lbAvailableIngredients.Location = new System.Drawing.Point(18, 134);
            this.lbAvailableIngredients.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lbAvailableIngredients.Name = "lbAvailableIngredients";
            this.lbAvailableIngredients.Size = new System.Drawing.Size(314, 130);
            this.lbAvailableIngredients.TabIndex = 7;
            this.lbAvailableIngredients.TabStop = false;
            // 
            // lblAddRecipeIngredientsCatalog
            // 
            this.lblAddRecipeIngredientsCatalog.AutoSize = true;
            this.lblAddRecipeIngredientsCatalog.Location = new System.Drawing.Point(18, 112);
            this.lblAddRecipeIngredientsCatalog.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAddRecipeIngredientsCatalog.Name = "lblAddRecipeIngredientsCatalog";
            this.lblAddRecipeIngredientsCatalog.Size = new System.Drawing.Size(181, 18);
            this.lblAddRecipeIngredientsCatalog.TabIndex = 6;
            this.lblAddRecipeIngredientsCatalog.Text = "Catalogue d\'ingrédients";
            // 
            // btnRemoveIngredient
            // 
            this.btnRemoveIngredient.Location = new System.Drawing.Point(344, 312);
            this.btnRemoveIngredient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRemoveIngredient.Name = "btnRemoveIngredient";
            this.btnRemoveIngredient.Size = new System.Drawing.Size(260, 32);
            this.btnRemoveIngredient.TabIndex = 5;
            this.btnRemoveIngredient.Text = "&Retirer ingrédient";
            this.btnRemoveIngredient.UseVisualStyleBackColor = true;
            this.btnRemoveIngredient.Click += new System.EventHandler(this.btnRemoveIngredient_Click);
            // 
            // lbSelectedIngredients
            // 
            this.lbSelectedIngredients.FormattingEnabled = true;
            this.lbSelectedIngredients.ItemHeight = 18;
            this.lbSelectedIngredients.Location = new System.Drawing.Point(18, 312);
            this.lbSelectedIngredients.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lbSelectedIngredients.Name = "lbSelectedIngredients";
            this.lbSelectedIngredients.Size = new System.Drawing.Size(314, 130);
            this.lbSelectedIngredients.TabIndex = 9;
            this.lbSelectedIngredients.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 289);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(254, 18);
            this.label1.TabIndex = 10;
            this.label1.Text = "Ingrédients de la nouvelle recette";
            // 
            // btnAddIngredient
            // 
            this.btnAddIngredient.Location = new System.Drawing.Point(344, 134);
            this.btnAddIngredient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddIngredient.Name = "btnAddIngredient";
            this.btnAddIngredient.Size = new System.Drawing.Size(260, 32);
            this.btnAddIngredient.TabIndex = 3;
            this.btnAddIngredient.Text = "&Ajouter à la recette";
            this.btnAddIngredient.UseVisualStyleBackColor = true;
            this.btnAddIngredient.Click += new System.EventHandler(this.btnAddIngredient_Click);
            // 
            // btnNewIngredient
            // 
            this.btnNewIngredient.Location = new System.Drawing.Point(344, 174);
            this.btnNewIngredient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnNewIngredient.Name = "btnNewIngredient";
            this.btnNewIngredient.Size = new System.Drawing.Size(260, 32);
            this.btnNewIngredient.TabIndex = 4;
            this.btnNewIngredient.Text = "&Nouvel ingrédient";
            this.btnNewIngredient.UseVisualStyleBackColor = true;
            this.btnNewIngredient.Click += new System.EventHandler(this.btnNewIngredient_Click);
            // 
            // AddRecipeForm
            // 
            this.AcceptButton = this.btnAddRecipeSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 525);
            this.Controls.Add(this.btnNewIngredient);
            this.Controls.Add(this.btnAddIngredient);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbSelectedIngredients);
            this.Controls.Add(this.btnRemoveIngredient);
            this.Controls.Add(this.lbAvailableIngredients);
            this.Controls.Add(this.lblAddRecipeIngredientsCatalog);
            this.Controls.Add(this.btnAddRecipeCancel);
            this.Controls.Add(this.btnAddRecipeSave);
            this.Controls.Add(this.tbAddRecipeDescription);
            this.Controls.Add(this.tbAddRecipeName);
            this.Controls.Add(this.lblAddRecipeDescription);
            this.Controls.Add(this.lblAddRecipeName);
            this.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "AddRecipeForm";
            this.Text = "Nouvelle recette";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAddRecipeName;
        private System.Windows.Forms.Label lblAddRecipeDescription;
        private System.Windows.Forms.TextBox tbAddRecipeName;
        private System.Windows.Forms.TextBox tbAddRecipeDescription;
        private System.Windows.Forms.Button btnAddRecipeSave;
        private System.Windows.Forms.Button btnAddRecipeCancel;
        private System.Windows.Forms.ListBox lbAvailableIngredients;
        private System.Windows.Forms.Label lblAddRecipeIngredientsCatalog;
        private System.Windows.Forms.Button btnRemoveIngredient;
        private System.Windows.Forms.ListBox lbSelectedIngredients;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddIngredient;
        private System.Windows.Forms.Button btnNewIngredient;
    }
}