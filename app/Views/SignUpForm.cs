﻿using app.Controllers;
using app.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace app.Views
{
    public partial class SignUpForm : Form
    {
        private UserController userController = new UserController();

        public SignUpForm()
        {
            InitializeComponent();
        }

        // show error box of paramterized message
        // clear inputs
        private void ShowErrorMessageAndClearTextBoxes(string message)
        {
            MessageBox.Show(
               message + " Veuillez réessayer.",
               "Echec de l'enregistrement",
               MessageBoxButtons.OK,
               MessageBoxIcon.Warning
           );
            this.tbUsername.Text = "";
            this.tbPassword.Text = "";
            this.tbPasswordConfirm.Text = "";
        }

        // handle signup attempt
        private void btnSignUp_Click(object sender, EventArgs e)
        {
            string username = this.tbUsername.Text;
            string password = this.tbPassword.Text;
            string passwordConfirmation = this.tbPasswordConfirm.Text;

            // check password confirmation (already checked in the form, this is for extra safety)
            if (password != passwordConfirmation)
                ShowErrorMessageAndClearTextBoxes("Confirmation du mot de passe incorrecte.");

            // check password length (already checked in the form, this is for extra safety)
            if (password.Length < 8)
                ShowErrorMessageAndClearTextBoxes("Mot de passe trop court.");

            // pass data to the UserController for SignUp(), show error if fail
            if (this.userController.SignUp(username, password, passwordConfirmation))
            {
                User currentUser = this.userController.GetUserByUsername(username);
                Globals.SetSignedInUser(currentUser);
                new HomeForm().Show();
                this.Hide();
            } else
            {
                this.ShowErrorMessageAndClearTextBoxes("Echec de l'enregistrement.");
            }
        }
    }
}
