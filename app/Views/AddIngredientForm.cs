﻿using app.DataSource;
using app.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace app.Views
{
    public partial class AddIngredientForm : Form
    {

        DataProvider dataProvider;

        public AddIngredientForm()
        {
            InitializeComponent();
            this.dataProvider = Program.GetDataProvider();
        }

        // persist new ingredient
        private void btnSave_Click(object sender, EventArgs e)
        {
            Ingredient newIngredient = new Ingredient(this.tbIngredientName.Text);
            dataProvider.AddIngredient(newIngredient);
            this.Close();
        }

        // close form
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
