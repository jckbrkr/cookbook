﻿namespace app.Views.Recipes
{
    partial class EditRecipeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEditRecipeCancel = new System.Windows.Forms.Button();
            this.btnEditRecipeSave = new System.Windows.Forms.Button();
            this.tbEditRecipeDescription = new System.Windows.Forms.TextBox();
            this.tbEditRecipeName = new System.Windows.Forms.TextBox();
            this.lblEditRecipeDescription = new System.Windows.Forms.Label();
            this.lblEditRecipeName = new System.Windows.Forms.Label();
            this.btnEditRecipeDelete = new System.Windows.Forms.Button();
            this.btnNewIngredient = new System.Windows.Forms.Button();
            this.btnAddIngredient = new System.Windows.Forms.Button();
            this.lblEditRecipeIngredients = new System.Windows.Forms.Label();
            this.lbSelectedIngredients = new System.Windows.Forms.ListBox();
            this.btnRemoveIngredient = new System.Windows.Forms.Button();
            this.lbAvailableIngredients = new System.Windows.Forms.ListBox();
            this.lblEditRecipeIngredientsCatalog = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnEditRecipeCancel
            // 
            this.btnEditRecipeCancel.Location = new System.Drawing.Point(312, 478);
            this.btnEditRecipeCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnEditRecipeCancel.Name = "btnEditRecipeCancel";
            this.btnEditRecipeCancel.Size = new System.Drawing.Size(291, 32);
            this.btnEditRecipeCancel.TabIndex = 7;
            this.btnEditRecipeCancel.Text = "Annuler";
            this.btnEditRecipeCancel.UseVisualStyleBackColor = true;
            this.btnEditRecipeCancel.Click += new System.EventHandler(this.btnEditRecipeCancel_Click);
            // 
            // btnEditRecipeSave
            // 
            this.btnEditRecipeSave.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditRecipeSave.Location = new System.Drawing.Point(18, 478);
            this.btnEditRecipeSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnEditRecipeSave.Name = "btnEditRecipeSave";
            this.btnEditRecipeSave.Size = new System.Drawing.Size(285, 32);
            this.btnEditRecipeSave.TabIndex = 6;
            this.btnEditRecipeSave.Text = "Enregistrer";
            this.btnEditRecipeSave.UseVisualStyleBackColor = true;
            this.btnEditRecipeSave.Click += new System.EventHandler(this.btnEditRecipeSave_Click);
            // 
            // tbEditRecipeDescription
            // 
            this.tbEditRecipeDescription.Location = new System.Drawing.Point(196, 46);
            this.tbEditRecipeDescription.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbEditRecipeDescription.Name = "tbEditRecipeDescription";
            this.tbEditRecipeDescription.Size = new System.Drawing.Size(404, 26);
            this.tbEditRecipeDescription.TabIndex = 2;
            // 
            // tbEditRecipeName
            // 
            this.tbEditRecipeName.Location = new System.Drawing.Point(196, 12);
            this.tbEditRecipeName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbEditRecipeName.Name = "tbEditRecipeName";
            this.tbEditRecipeName.Size = new System.Drawing.Size(404, 26);
            this.tbEditRecipeName.TabIndex = 1;
            // 
            // lblEditRecipeDescription
            // 
            this.lblEditRecipeDescription.AutoSize = true;
            this.lblEditRecipeDescription.Location = new System.Drawing.Point(18, 49);
            this.lblEditRecipeDescription.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEditRecipeDescription.Name = "lblEditRecipeDescription";
            this.lblEditRecipeDescription.Size = new System.Drawing.Size(90, 18);
            this.lblEditRecipeDescription.TabIndex = 7;
            this.lblEditRecipeDescription.Text = "Description";
            // 
            // lblEditRecipeName
            // 
            this.lblEditRecipeName.AutoSize = true;
            this.lblEditRecipeName.Location = new System.Drawing.Point(18, 17);
            this.lblEditRecipeName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEditRecipeName.Name = "lblEditRecipeName";
            this.lblEditRecipeName.Size = new System.Drawing.Size(44, 18);
            this.lblEditRecipeName.TabIndex = 6;
            this.lblEditRecipeName.Text = "Nom";
            // 
            // btnEditRecipeDelete
            // 
            this.btnEditRecipeDelete.Location = new System.Drawing.Point(18, 518);
            this.btnEditRecipeDelete.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnEditRecipeDelete.Name = "btnEditRecipeDelete";
            this.btnEditRecipeDelete.Size = new System.Drawing.Size(585, 32);
            this.btnEditRecipeDelete.TabIndex = 8;
            this.btnEditRecipeDelete.Text = "Supprimer";
            this.btnEditRecipeDelete.UseVisualStyleBackColor = true;
            this.btnEditRecipeDelete.Click += new System.EventHandler(this.btnEditRecipeDelete_Click);
            // 
            // btnNewIngredient
            // 
            this.btnNewIngredient.Location = new System.Drawing.Point(344, 169);
            this.btnNewIngredient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnNewIngredient.Name = "btnNewIngredient";
            this.btnNewIngredient.Size = new System.Drawing.Size(260, 32);
            this.btnNewIngredient.TabIndex = 4;
            this.btnNewIngredient.Text = "Nouvel ingrédient";
            this.btnNewIngredient.UseVisualStyleBackColor = true;
            this.btnNewIngredient.Click += new System.EventHandler(this.btnNewIngredient_Click);
            // 
            // btnAddIngredient
            // 
            this.btnAddIngredient.Location = new System.Drawing.Point(344, 128);
            this.btnAddIngredient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddIngredient.Name = "btnAddIngredient";
            this.btnAddIngredient.Size = new System.Drawing.Size(260, 32);
            this.btnAddIngredient.TabIndex = 3;
            this.btnAddIngredient.Text = "&Ajouter à la recette";
            this.btnAddIngredient.UseVisualStyleBackColor = true;
            this.btnAddIngredient.Click += new System.EventHandler(this.btnAddIngredient_Click);
            // 
            // lblEditRecipeIngredients
            // 
            this.lblEditRecipeIngredients.AutoSize = true;
            this.lblEditRecipeIngredients.Location = new System.Drawing.Point(18, 286);
            this.lblEditRecipeIngredients.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEditRecipeIngredients.Name = "lblEditRecipeIngredients";
            this.lblEditRecipeIngredients.Size = new System.Drawing.Size(188, 18);
            this.lblEditRecipeIngredients.TabIndex = 17;
            this.lblEditRecipeIngredients.Text = "Ingérdients de la recette";
            // 
            // lbSelectedIngredients
            // 
            this.lbSelectedIngredients.FormattingEnabled = true;
            this.lbSelectedIngredients.ItemHeight = 18;
            this.lbSelectedIngredients.Location = new System.Drawing.Point(18, 309);
            this.lbSelectedIngredients.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lbSelectedIngredients.Name = "lbSelectedIngredients";
            this.lbSelectedIngredients.Size = new System.Drawing.Size(314, 130);
            this.lbSelectedIngredients.TabIndex = 16;
            this.lbSelectedIngredients.TabStop = false;
            // 
            // btnRemoveIngredient
            // 
            this.btnRemoveIngredient.Location = new System.Drawing.Point(344, 309);
            this.btnRemoveIngredient.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRemoveIngredient.Name = "btnRemoveIngredient";
            this.btnRemoveIngredient.Size = new System.Drawing.Size(260, 32);
            this.btnRemoveIngredient.TabIndex = 5;
            this.btnRemoveIngredient.Text = "Retirer ingrédient";
            this.btnRemoveIngredient.UseVisualStyleBackColor = true;
            this.btnRemoveIngredient.Click += new System.EventHandler(this.btnRemoveIngredient_Click);
            // 
            // lbAvailableIngredients
            // 
            this.lbAvailableIngredients.FormattingEnabled = true;
            this.lbAvailableIngredients.ItemHeight = 18;
            this.lbAvailableIngredients.Location = new System.Drawing.Point(18, 128);
            this.lbAvailableIngredients.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lbAvailableIngredients.Name = "lbAvailableIngredients";
            this.lbAvailableIngredients.Size = new System.Drawing.Size(314, 130);
            this.lbAvailableIngredients.TabIndex = 14;
            this.lbAvailableIngredients.TabStop = false;
            // 
            // lblEditRecipeIngredientsCatalog
            // 
            this.lblEditRecipeIngredientsCatalog.AutoSize = true;
            this.lblEditRecipeIngredientsCatalog.Location = new System.Drawing.Point(18, 106);
            this.lblEditRecipeIngredientsCatalog.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblEditRecipeIngredientsCatalog.Name = "lblEditRecipeIngredientsCatalog";
            this.lblEditRecipeIngredientsCatalog.Size = new System.Drawing.Size(181, 18);
            this.lblEditRecipeIngredientsCatalog.TabIndex = 13;
            this.lblEditRecipeIngredientsCatalog.Text = "Catalogue d\'ingrédients";
            // 
            // EditRecipeForm
            // 
            this.AcceptButton = this.btnEditRecipeSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(621, 559);
            this.Controls.Add(this.btnNewIngredient);
            this.Controls.Add(this.btnAddIngredient);
            this.Controls.Add(this.lblEditRecipeIngredients);
            this.Controls.Add(this.lbSelectedIngredients);
            this.Controls.Add(this.btnRemoveIngredient);
            this.Controls.Add(this.lbAvailableIngredients);
            this.Controls.Add(this.lblEditRecipeIngredientsCatalog);
            this.Controls.Add(this.btnEditRecipeDelete);
            this.Controls.Add(this.btnEditRecipeCancel);
            this.Controls.Add(this.btnEditRecipeSave);
            this.Controls.Add(this.tbEditRecipeDescription);
            this.Controls.Add(this.tbEditRecipeName);
            this.Controls.Add(this.lblEditRecipeDescription);
            this.Controls.Add(this.lblEditRecipeName);
            this.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "EditRecipeForm";
            this.Text = "Modifier la recette";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEditRecipeCancel;
        private System.Windows.Forms.Button btnEditRecipeSave;
        private System.Windows.Forms.TextBox tbEditRecipeDescription;
        private System.Windows.Forms.TextBox tbEditRecipeName;
        private System.Windows.Forms.Label lblEditRecipeDescription;
        private System.Windows.Forms.Label lblEditRecipeName;
        private System.Windows.Forms.Button btnEditRecipeDelete;
        private System.Windows.Forms.Button btnNewIngredient;
        private System.Windows.Forms.Button btnAddIngredient;
        private System.Windows.Forms.Label lblEditRecipeIngredients;
        private System.Windows.Forms.ListBox lbSelectedIngredients;
        private System.Windows.Forms.Button btnRemoveIngredient;
        private System.Windows.Forms.ListBox lbAvailableIngredients;
        private System.Windows.Forms.Label lblEditRecipeIngredientsCatalog;
    }
}