﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using System.Windows.Forms;

using app.Models;
using app.DataSource;
using app.Views.Recipes;

namespace app.Views
{
    public partial class RecipesForm : Form
    {
        private DataProvider dataProvider;

        private Recipe selectedRecipe;
        private User currentUser;

        private BindingList<Recipe> nonFavoriteRecipes;
        private BindingList<Recipe> favoriteRecipes;

        public RecipesForm()
        {
            InitializeComponent();
            InitializeForm();
        }

        public void InitializeForm()
        {
            // get reference to data provider
            this.dataProvider = Program.GetDataProvider();

            // get reference to current user
            this.currentUser = Globals.GetSignedInUser();

            // handle favorite and non favorite recipes
            this.handleFavoriteRecipes();
        }

        public void handleFavoriteRecipes()
        {
            // get user favorite recipes
            this.favoriteRecipes = this.currentUser.GetFavoriteRecipes();

            // get filtered recipes
            BindingList<Recipe> allRecipes = this.dataProvider.GetRecipes();
            this.nonFavoriteRecipes = new BindingList<Recipe>();
            foreach (Recipe r in allRecipes)
            {
                if (!this.favoriteRecipes.Contains(r))
                {
                    this.nonFavoriteRecipes.Add(r);
                }
            }

            // set DataSource for ListBoxes
            this.lbFavoriteRecipes.DataSource = this.favoriteRecipes;
            this.lbRecipes.DataSource = this.nonFavoriteRecipes;
        }

        // update recipe detail
        public void UpdateRecipeData(Recipe recipe)
        {
            this.selectedRecipe = recipe;
            this.lblRecipeName.Text = recipe.name;
            this.lblRecipeDescription.Text = recipe.description;
            this.lbRecipeIngredients.DataSource = recipe.ingredients;
            if (this.currentUser.GetFavoriteRecipes().Contains(this.selectedRecipe))
            {
                this.chbFavorite.Checked = true;
            } else {
                this.chbFavorite.Checked = false;
            }
            this.chbFavorite.Focus();
        }

        // handle favorite toggle
        private void chbFavorite_Click(object sender, EventArgs e)
        {
            if (this.chbFavorite.Checked)
            {
                // recipe is added to favorites
                this.selectedRecipe = (Recipe)this.lbRecipes.SelectedItem;
                this.currentUser.AddFavoriteRecipe(this.selectedRecipe);
            } else
            {
                // recipe is removed from favorites
                this.selectedRecipe = (Recipe)this.lbFavoriteRecipes.SelectedItem;
                this.currentUser.RemoveFavoriteRecipe(this.selectedRecipe);
            }
        }

        // handle recipe focus change
        private void lbRecipes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.nonFavoriteRecipes.Count == 0) return;
            this.UpdateRecipeData((Recipe)lbRecipes.SelectedItem);
        }

        // handle favorite recipe focus change
        private void lbFavoriteRecipes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.favoriteRecipes.Count == 0) return;
            this.UpdateRecipeData((Recipe)lbFavoriteRecipes.SelectedItem);
        }

        // handle add button click
        private void btnAddRecipe_Click(object sender, EventArgs e)
        {
            this.Close();
            new AddRecipeForm(this).Show();
        }

        // handle edit button click
        private void btnEditRecipe_Click(object sender, EventArgs e)
        {
            this.Close();
            new EditRecipeForm(this.selectedRecipe).Show();
        }

        // handle return button click
        private void btnRecipesReturn_Click(object sender, EventArgs e)
        {
            this.Close();
            new HomeForm().Show();
        }
    }
}
