﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using app.Controllers;
using app.Models;

namespace app.Views
{
    public partial class LoginForm : Form
    {
        UserController userController = new UserController();

        public LoginForm()
        {
            InitializeComponent();
        }

        // handle login attempt
        private void btnLogin_Click(object sender, EventArgs e)
        {
            string username = this.tbUsername.Text;
            string password = this.tbPassword.Text;
            // set signed-in user if success of password check
            if (this.userController.CheckUsernameAndPassword(username, password))
            {
                User currentUser = this.userController.GetUserByUsername(username);
                Globals.SetSignedInUser(currentUser);
                new HomeForm().Show();
                this.Hide();
            } else {
                // else show error message and clear inputs
                MessageBox.Show("Nom d'utilisateur ou mot de passe incorrect. Veuillez réessayer.", "Echec d'authentification",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.tbUsername.Text = "";
                this.tbPassword.Text = "";
            }
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            new SignUpForm().Show();
            this.Close();
        }
        
    }
}
