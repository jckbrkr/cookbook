﻿using System.ComponentModel;

namespace app.Models
{
    public class Recipe
    {
        public int id;
        public string name { get; set; }
        public string description { get; set; }
        public BindingList<Ingredient> ingredients { get; set; }

        public Recipe(int id, string name, string description, BindingList<Ingredient> ingredients) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.ingredients = ingredients;
        }

        override public string ToString()
        {
            return this.name + "(" + this.id + ")";
        }
       
    }
}
