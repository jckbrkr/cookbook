﻿using System.ComponentModel;

namespace app.Models
{
    public class User
    {
        private string username;
        private string hashedPassword;
        private bool isAdmin;
        private readonly BindingList<Recipe> favoriteRecipes;

        public User(string username, string hashedPassword, bool isAdmin) {
            this.username = username;
            this.hashedPassword = hashedPassword;
            this.isAdmin = isAdmin;
            this.favoriteRecipes = new BindingList<Recipe>();
        }

        public string GetUsername()
        {
            return this.username;
        }

        public string GetHashedPassword()
        {
            return this.hashedPassword;
        }

        public void SetHashedPassword(string hashedPassword)
        {
            this.hashedPassword = hashedPassword;
        }

        public BindingList<Recipe> GetFavoriteRecipes()
        {
            return this.favoriteRecipes;
        }

        public void AddFavoriteRecipe(Recipe recipe)
        {
            this.favoriteRecipes.Add(recipe);
        }

        public void RemoveFavoriteRecipe(Recipe recipe)
        {
            this.favoriteRecipes.Remove(recipe);
        }
    }
}
