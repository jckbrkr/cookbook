﻿namespace app.Models
{
    public class Ingredient
    {
        private string name;
            
        public Ingredient(string name) {
            this.name = name;
        }

        override public string ToString()
        {
            return this.name;
        }
    }
}
