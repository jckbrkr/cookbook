﻿using System;
using System.ComponentModel;
using app.Models;

namespace app.DataSource
{
    public class DataProvider : DataProviderInterface
    {
        public BindingList<Recipe> recipes = new BindingList<Recipe>();
        public BindingList<User> users = new BindingList<User>();
        public BindingList<Ingredient> ingredients = new BindingList<Ingredient>();
        private Random random = new Random();

        public DataProvider()
        {
            this.InitializeIngredients();
            this.IntializeRecipes();
            this.InitializeUsers();
            foreach (Recipe r in this.recipes)
            {
                Console.WriteLine(r.ingredients);
            }
        }
        
        public void IntializeRecipes()
        {
            this.recipes.Add(
                new Recipe(
                    0,
                   "Sweet Potatoes with Apple Butter",
                   "Creamy sweet potatoes mashed with fruity apple butter make for the perfect fall side dish.",
                   new BindingList<Ingredient>(
                       new BindingList<Ingredient> {
                           this.ingredients[0],
                           this.ingredients[1],
                           this.ingredients[2]
                       }
                    )
                )
             );

            this.recipes.Add(
                new Recipe(
                    1,
                    "Old-Fashioned Apple Pie",
                    "This is a purists' pie that tastes of nothing but apples, with just a hint of spice and butter.",
                   new BindingList<Ingredient>(
                       new BindingList<Ingredient> {
                           this.ingredients[2],
                           this.ingredients[7],
                           this.ingredients[3]
                       }
                    )
                )
            );

            this.recipes.Add(
                new Recipe(
                    2,
                    "Beef Stew in Red Wine Sauce",
                    "Deeply flavorful and packed with tender chunks of flatiron steak, this is the quintessential beef stew.",
                        new BindingList<Ingredient>(
                            new BindingList<Ingredient> {
                            this.ingredients[6],
                            this.ingredients[5],
                            this.ingredients[3]
                        }
                    )
                )
            );

            this.recipes.Add(
                new Recipe(
                    3,
                    "Butternut Squash Soup with Crisp Pancetta",
                    "Sweet and creamy squash soup gets a delicious salty crunch from crispy pancetta.",
                    new BindingList<Ingredient>(
                        new BindingList<Ingredient> {
                            this.ingredients[2],
                            this.ingredients[4],
                            this.ingredients[3]
                        }
                    )
                )
            );
        }

        public void InitializeUsers()
        {
            this.users.Add(new User("admin", "$2a$10$uH4e/W6eDfshilQm9GlwbeEieMbWwX8nE7Iwij9eg3RMgymFvNjM6", true));
        }

        public void InitializeIngredients()
        {
            this.ingredients.Add(new Ingredient("Broccoli"));
            this.ingredients.Add(new Ingredient("Aubergine"));
            this.ingredients.Add(new Ingredient("Carotte"));
            this.ingredients.Add(new Ingredient("Boeuf"));
            this.ingredients.Add(new Ingredient("Poulet"));
            this.ingredients.Add(new Ingredient("Sel"));
            this.ingredients.Add(new Ingredient("Sucre"));
            this.ingredients.Add(new Ingredient("Sauce soja"));
        }

        public BindingList<User> GetUsers()
        {
            return this.users;
        }

        public BindingList<Ingredient> GetIngredients()
        {
            return this.ingredients;
        }

        public BindingList<Recipe> GetRecipes()
        {
            return this.recipes;
        }

        public void AddRecipe(Recipe recipe)
        {
            this.recipes.Add(recipe);
        }

        public void AddUser(User user)
        {
            this.users.Add(user);
        }

        public void AddIngredient(Ingredient ingredient)
        {
            this.ingredients.Add(ingredient);
        }
    }
}
