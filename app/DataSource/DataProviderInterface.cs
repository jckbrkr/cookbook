﻿using System.ComponentModel;
using app.Models;

namespace app.DataSource
{
    public interface DataProviderInterface
    {
        BindingList<User> GetUsers();
        BindingList<Ingredient> GetIngredients();
        BindingList<Recipe> GetRecipes();
        void AddRecipe(Recipe recipe);
    }
}