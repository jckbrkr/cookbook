﻿using app.DataSource;
using app.Models;
using System.ComponentModel;


namespace app.Controllers
{
    class UserController
    {
        // has reference to the DataProvider
        DataProvider dataProvider = Program.GetDataProvider();

        // returns true on successful credential verification
        public bool CheckUsernameAndPassword(string username, string password)
        {
            BindingList<User> users = this.dataProvider.GetUsers();
            foreach (User u in users)
            {
                if (username == u.GetUsername())
                {
                    string hashedPassword = BCrypt.Net.BCrypt.HashPassword(password);
                    //Console.WriteLine(hashedPassword);
                    return BCrypt.Net.BCrypt.Verify(password, hashedPassword);
                }
            }
            return false;
        }

        // returns true on successful user creation
        public bool SignUp(string username, string password, string passwordConfirmation)
        {   
            // check password confirmation (already checked in the form, this is for extra safety)
            if (password != passwordConfirmation)
            {
                return false;
            }

            // check password length (already checked in the form, this is for extra safety)
            if (password.Length < 8) return false;

            // check that the username is not taken
            BindingList<User> users = this.dataProvider.GetUsers();
            foreach (User u in users)
            {
                if (username == u.GetUsername()) return false;
            }

            // create user
            string hashedPassword = BCrypt.Net.BCrypt.HashPassword(password);
            User newUser = new User(username, hashedPassword, false);
            this.dataProvider.AddUser(newUser);
            return true;
        }

        // get user by unique username
        public User GetUserByUsername(string username)
        {
            BindingList<User> users = this.dataProvider.GetUsers();
            foreach (User u in users)
            {
                if (username == u.GetUsername())
                {
                    return u;
                }
            }
            return null;
        }

        // password change
        // this func doesn't check anything, assumes user is authentificated
        public bool SetUserPassword(string username, string password)
        {
            BindingList<User> users = this.dataProvider.GetUsers();
            User u = this.GetUserByUsername(username);
            string hashedPassword = BCrypt.Net.BCrypt.HashPassword(password);
            u.SetHashedPassword(hashedPassword);
            return true;
        }
    }
}
