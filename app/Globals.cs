﻿using app.DataSource;
using app.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app
{
    static class Globals
    {
        static public string appName = "Cookbook";

        private static User signedInUser;

        public static User GetSignedInUser()
        {
            return signedInUser;
        }

        public static void SetSignedInUser(User user)
        {
            signedInUser = user;
        }
    }
}
